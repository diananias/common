<?php

namespace RB\Common\Providers;

use Illuminate\Support\ServiceProvider;
use RB\Common\Domain\Armazenamento\ArmazenamentoService;
use RB\Common\Domain\Armazenamento\ArmazenamentoServiceInterface;
use RB\Common\Domain\Criptografia\CriptografiaService;
use RB\Common\Domain\Criptografia\CriptografiaServiceInterface;
use RB\Common\Domain\Fila\FilaService;
use RB\Common\Domain\Fila\FilaServiceInterface;
use RB\Common\Domain\HttpClient\HttpClientService;
use RB\Common\Domain\HttpClient\HttpClientServiceInterface;
use RB\Common\Domain\Log\LogService;
use RB\Common\Domain\Log\LogServiceInterface;

/**
 * Class CommonServiceProvider
 * @package RB\Common\Providers
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class CommonServiceProvider extends ServiceProvider
{
    public function boot()
    {
        
    }

    public function register()
    {
        $this->app->bind(ArmazenamentoServiceInterface::class, ArmazenamentoService::class);
        $this->app->bind(CriptografiaServiceInterface::class, CriptografiaService::class);
        $this->app->bind(FilaServiceInterface::class, FilaService::class);
        $this->app->bind(HttpClientServiceInterface::class, HttpClientService::class);
        $this->app->bind(LogServiceInterface::class, LogService::class);
    }
}