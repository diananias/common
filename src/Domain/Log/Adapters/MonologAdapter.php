<?php

namespace RB\Common\Domain\Log\Adapters;

use RB\Common\Domain\Log\LogServiceInterface;

/**
 * Class MonologAdapter
 * @package RB\Common\Domain\Log\Adapters
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class MonologAdapter implements LogServiceInterface
{

}