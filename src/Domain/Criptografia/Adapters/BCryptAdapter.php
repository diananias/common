<?php

namespace RB\Common\Domain\Criptografia\Adapters;

use RB\Common\Domain\Criptografia\CriptografiaServiceInterface;

/**
 * Class BCryptAdapter
 * @package RB\Common\Domain\Criptografia\Adapters
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class BCryptAdapter implements CriptografiaServiceInterface
{

}