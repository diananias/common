<?php

namespace RB\Common\Domain\Criptografia\Adapters;

use RB\Common\Domain\Criptografia\CriptografiaServiceInterface;

/**
 * Class Sha1Adapter
 * @package RB\Common\Domain\Criptografia\Adapters
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class Sha1Adapter implements CriptografiaServiceInterface
{

}