<?php

namespace RB\Common\Domain\Armazenamento\Adaters;

use RB\Common\Domain\Armazenamento\ArmazenamentoServiceInterface;

/**
 * Class S3Adapter
 * @package RB\Common\Domain\Armazenamento\Adaters
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class S3Adapter implements ArmazenamentoServiceInterface
{
}