<?php

namespace RB\Common\Domain\Armazenamento\Adaters;

use RB\Common\Domain\Armazenamento\ArmazenamentoServiceInterface;

/**
 * Class AzureBlobStorageAdapter
 * @package RB\Common\Domain\Armazenamento\Adaters
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class AzureBlobStorageAdapter implements ArmazenamentoServiceInterface
{

}