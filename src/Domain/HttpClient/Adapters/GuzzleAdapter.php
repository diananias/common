<?php

namespace RB\Common\Domain\HttpClient\Adapters;

use RB\Common\Domain\HttpClient\HttpClientServiceInterface;

/**
 * Class GuzzleAdapter
 * @package RB\Common\Domain\HttpClient\Adapters
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class GuzzleAdapter implements HttpClientServiceInterface
{

}