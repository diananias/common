<?php

namespace RB\Common\Domain\Fila\Adapters;

use RB\Common\Domain\Fila\FilaServiceInterface;

/**
 * Class ServiceBusAdapter
 * @package RB\Common\Domain\Fila\Adapters
 * @author Diego Ananias <diego.ananias@rb.com.br>
 * @copyright RB Serviços
 */
class ServiceBusAdapter implements FilaServiceInterface
{

}